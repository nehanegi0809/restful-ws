package org.testapp.restful.model;
 
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
 
@XmlRootElement(name = "user")
@XmlAccessorType(XmlAccessType.FIELD)
public class User {
 
	private String id;
    private String firstname;
    private String lastname;
    private String birthday;
    private String email;
    private String password;
 
    // This default constructor is required if there are other constructors.
    public User() {
 
    }
 
    public User(String id,String firstname, String lastname, String birthday, String email, String password) {
        this.id=id;
    	this.firstname=firstname;
        this.lastname = lastname;
        this.birthday=birthday;
        this.email=email;
        this.password=password;
    }
    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
 
}