package org.testapp.restful.dao;
 
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
 
import org.testapp.restful.model.User;
 
public class UserDAO {
 
    private static final Map<String, User> empMap = new HashMap<String, User>();
 
    static {
        initUsers();
    }
 
    private static void initUsers() {
        User u1 = new User("U01", "Smith", "Clark","01.01.1990","smith.clark@buuz.com","*****");
        User u2 = new User("U02", "Allen", "Sasman","01.01.1990","allen.sasman@buuz.com","*****");
        User u3 = new User("U03", "Jones", "Mannr","01.01.1990","jones.mannr@buuz.com","*****");
 
        empMap.put(u1.getId(), u1);
        empMap.put(u2.getId(), u2);
        empMap.put(u3.getId(), u3);
    }
 
    public static User getUser(String id) {
        return empMap.get(id);
    }
 
    public static User addUser(User usr) {
        empMap.put(usr.getId(), usr);
        return usr;
    }
 
    public static User updateUser(User usr) {
        empMap.put(usr.getId(), usr);
        return usr;
    }
 
    public static List<User> getAllUser() {
        Collection<User> c = empMap.values();
        List<User> list = new ArrayList<User>();
        list.addAll(c);
        return list;
    }
     
    List<User> list;
 
}