package org.testapp.restful.service;
 
import java.util.List;
 
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
 
import org.testapp.restful.dao.UserDAO;
import org.testapp.restful.model.User;
 
@Path("/users")
public class UserService {
 
    // URI:
    // /contextPath/servletPath/user
    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<User> getUSers_JSON() {
        List<User> listOfUsers = UserDAO.getAllUser();
        return listOfUsers;
    }
 
    // URI:
    // /contextPath/servletPath/user/{id}
    @GET
    @Path("/{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public User getUser(@PathParam("id") String id) {
        return UserDAO.getUser(id);
    }
 
    // URI:
    // /contextPath/servletPath/user
    @POST
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public User addUser(User usr) {
        return UserDAO.addUser(usr);
    }
 
    // URI:
    // /contextPath/servletPath/user
    @PUT
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public User updateUser(User usr) {
        return UserDAO.updateUser(usr);
    }
}